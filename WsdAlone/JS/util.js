function LG() { console.log(arguments); };
function LGERR(errMsg) { console.error("Code ERROR:" + errMsg); };
(function() {

if (typeof GLOB == 'undefined' ) GLOB = { map: null, page : null, mapPanel : null};

var config;
setTimeout( function() { config = UT.config; },0);

window.UT = {
    config : {
        notify: { delay : 3000 }
    },
    Host: 'http://' + document.location.host + '/',
    DATA: {"Devices": {}, "Channels": {}}, // Static storage of devices and channels
    Stor: function(dataId, auxId) {
        var _data = UT.DATA[ (dataId == 'Devices') ? 'Devices' : 'Channels' ] ;

        var storageTypes = {
            Devices : {
                load: function(data, overwrite) {
                    overwrite = typeof overwrite != 'undefined' && overwrite;
                    for (var i=0; i<data.length; i++) {
                        data[i].lon = data[i].Long4Dec;
                        data[i].lat = data[i].Lat4Dec;
                        delete (data[i].Long4Dec);
                        delete (data[i].Lat4Dec);

                        if (typeof _data[data[i]['Id']] == 'undefined' || overwrite) 
                            _data[data[i]['Id']] = data[i];
                    }
                },
                get:            function(id) { return _data[id]; },
                getChannels:    function(id) { return typeof _data[id] == 'undefined' ? null : _data[id].Chans; }
            },

            Channels : {
                load: function(data) {
                    for (var i=0; i<data.length; i++) {
                        var chanObj = _data[data[i].Channel];
                        if (typeof chanObj == 'undefined')
                            _data[data[i].Channel] = data[i];
                        else
                            angular.extend(chanObj, data[i]);
                    }
                },
                addDevice: function(chanId, devObj) {
                    if (typeof _data[chanId] == 'undefined') 
                        _data[chanId] = {Devices: [devObj]};

                    if (typeof _data[chanId].Devices == 'undefined') 
                        _data[chanId].Devices = [devObj];
                    else if (_data[chanId].Devices.indexOf(devObj) == -1)
                        _data[chanId].Devices.push(devObj);
                },
                getDevices: function(id) { 
                    return typeof _data[id][dataId] == 'undefined' ? null : _data[id].Devices;
                }
            }
        };

        storageTypes[dataId].get = function(id) { 
            if (typeof auxId == 'undefined')
                return typeof _data[id] == 'undefined' ? null : _data[id];
            else 
                return typeof _data[id][auxId] == 'undefined' ? null : _data[id][auxId];

        };

        return storageTypes[dataId];
    },
    Ajax: function() {
        var fullUrl = this.Host + (typeof noMVC != 'undefined' && noMVC ? '/noMVC/' : '/Api/');

        var getExt = function(url, cb, pt1, pt2, opt1, opt2) {
            var url1 = fullUrl + url + qString(url, pt1, pt2, opt1, opt2); 
            Ext.Ajax.request({ 
                url: fullUrl + url + qString(url, pt1, pt2, opt1, opt2), 
                success: function(data) {
                    var res = Ext.decode(data.responseText);
                    var ret = setData(url,res);
                    cb(ret);
                }, 
                fail: function(data) {
                }
            });
         };

        var get = function(url, cb, pt1, pt2, opt1, opt2) {
            var url1 = fullUrl + url + qString(url, pt1, pt2, opt1, opt2); 
            jQuery.ajax({ 
                url: fullUrl + url + qString(url, pt1, pt2, opt1, opt2), 
                success: function(data) {
                //    var res = Ext.decode(data.responseText);
                    try {
                        var res = JSON.parse(data);
                        var ret = setData(url,res);
                        cb(ret);
                    } catch (e) {}
                }, 
                fail: function(data) {
                }
            });
         };

         var qString = function(url, pt1, pt2, opt1, opt2) {
            ret = "";
            switch (url) {
                case 'WsTvwsLookup' :
                    ret = '?k=a&lo=' + pt1.lon + '&la=' + pt1.lat;
                    break;
                case 'WsDeviceList' :
                    ret = '?k=a&lonMin=' + pt1.lon + '&latMin=' + pt1.lat +
                              '&lonMax=' + pt2.lon + '&latMax=' + pt2.lat;
                    break;
                case 'WsDeviceDetails':
                    ret = '?k=a&ids=1,2,3';
                    break;
                case 'WsChannelsList':
                    ret = '?k=a';
                    break;
            };
            return ret;
         };

        // TODO
        var mockDataDetail = function( res ) {
            for (var i=0; i<res.length; i++){
                var dt = res[i].Chans;
                for (var j=0; j<dt.length; j++) {
                    if (false) // TODO
                    if (dt[j].Pwr == -7 && Math.random() > 0.6)
                        res[i].Chans[j].Pwr = 50;
                    res[i].Chans[j].Pwr += 30;
                }
            }
            return res;
        };

         var setData = function(url, res){
            var ret = [];
            switch (url) {
                case 'WsDeviceList' :
                    UT.Stor('Devices').load(res);
                    for( var i=0; i<res.length; i++)
                        ret.push([res[i].lon, res[i].lat, res[i].Id]);

                    break;
                case 'WsDeviceDetails':
                    res = mockDataDetail(res);
                    UT.Stor('Devices').load(res, true);
                    for (var i=0; i<res.length; i++)
                        for (var j=0; j<res[i].Chans.length; j++) {
                            UT.Stor('Channels').addDevice( res[i].Chans[j].Chan, 
                                {   'Id': res[i].Id, 
                                    'lon' : res[i].lon, 
                                    'lat': res[i].lat, 
                                    'Pwr': res[i].Chans[j].Pwr
                               }
                            );
                        }

                    ret = res;
                    break;
                case 'WsTvwsLookup' :
                    UT.Stor('Channels').load(res);
                    ret = res;
                    break;
                case 'WsChannelsList':
                    UT.Stor('Channels').load(res);
                    ret = '?k=a';
                    break;
                default: ret = res;
            }
            return ret;
         };

         return {
            get: get,
            layerData: setData
         }
    },
    NGRegistry: {},
    NG: function(targetPaneId) { 
        var targetPane  = document.getElementById(targetPaneId)
        return {
            inject: function(divId, data) {
                var el = document.getElementById(divId);
                var scope =  angular.element(el).scope();
                UT.NGRegistry[divId] = scope;
                targetPane.appendChild(el);
                if (typeof data != 'undefined' && typeof data == 'object') {
                    scope.data = data;
                    scope.init();
                    scope.$digest();
                }
                return scope;
            },
            update: function(directive, key, val) {
                if (typeof UT.NGRegistry[directive] != 'undefined') {
                    UT.NGRegistry[directive][key] = val;
                    UT.NGRegistry[directive].$digest();
                }
            },
            callFn : function( func, args) {
                $scope = UT.NGRegistry[targetPaneId];
                if (typeof $scope != 'undefined') {
                    $scope[func](args);
                } else {
                    $scope = { '$digest' : function() {
                        LGERR('Scope was undefined when calling ' + func + ' for ' + targetPaneId); } 
                    };
                }
                return $scope;
            }
        }
    },
    Ctrl: function(ctrl) {
        return {
            addLayer: function(layer) {
                var ctrlLayers = ctrl.layers;
                ctrlLayers.push(layer);
                ctrl.setLayer(ctrlLayers);
            }
        }
    },
    LonLat: function(lonLat) {
        return new OpenLayers.LonLat(lonLat[0], lonLat[1]); 
    },
    Popup: function(popId) {
        var pop = null;
        if (typeof popId == 'string')
            for(var i=0; i<map.popups.length; i++){
                if(GLOB.map.popups[i].id == popId) {
                    pop = GLOB.map.popups[i]; break;
                }
            } 

        return {
            P : pop,
            mkFrameCloud: function(loc, html){
                this.P = new OpenLayers.Popup.FramedCloud(popId, loc, null, html, null, true);
            },
            remove: function() {
                if (pop != null) { 
                    GLOB.map.removePopup(pop);
                    pop.destroy();
                }
                this.P = null;
            },
            update: function(loc, html) {
                //if (pop != null) this.remove();
                this.mkFrameCloud(loc, '...');
                var inject = document.getElementById('ngdevice');
                var cd = this.P.contentDiv;
                setTimeout(function() {
                    angular.element(cd).append(inject);
                }, 0);

                return this;
            }
        }
    },
    Shape: function() {
        var Point = {
            add: function(lon, lat){
                return new OpenLayers.Geometry.Point(lon, lat);
            }
        };

        var PointList = {
            add: function(l, closeIt) {
                var poly = [];
                for (var i=0; i< l.length; i++) {
                    poly.push( new OpenLayers.Geometry.Point(l[i][0], l[i][1]));
                }

                if (typeof closeIt!= 'undefined' && closeIt) {
                    if (l[0][0] != l[l.length-1][0] || l[0][1] != l[l.length-1][1])
                        poly.push(poly[0]);
                }
                return {o: this, v:poly};
            }
        };
        
        var LineString = {
            add: function(l) {
                var pList = PointList.add(l, false).v;
                return new OpenLayers.Geometry.LineString(pList);
            }, 
            addFV: function(l, style){
                return new OpenLayers.Feature.Vector( this.add(l), null, style );
            },
            createLV: function(id, l, style) {
                var v = new OpenLayers.Layer.Vector(id);
                v.addFeatures([this.addFV(l, style)]);
                return v;
            }
        };

        var LineRing = {
            add: function(l) {
                var pList = PointList.add(l, true).v;
                return new OpenLayers.Geometry.LinearRing(pList);
            }, 
            addFV: function(l, style){
                return new OpenLayers.Feature.Vector( this.add(l), null, style );
            },
            createLV: function(id, l, style) {
                var v = new OpenLayers.Layer.Vector(id, { rendererOptions: {zIndexing: true} });
                v.addFeatures([this.addFV(l, style)]);
                return v;
            }
        };

        var Markers = {
            addFV: function(points, style) {
                var pointList = [];
                for (var i=0; i<points.length; i++) {
                    var feature = new OpenLayers.Feature.Vector(
                        new OpenLayers.Geometry.Point(points[i][0], points[i][1], style)
                    );
                    feature.style = style;
                    //feature.attributes = {'devId' : points[0][2], 'chan': points[0][3] };
                    feature.attributes = {'devId' : points[i][2], 'chan': points[i][3] };
                    pointList.push(feature);
                }
                return pointList;
            }
        };

        var Polygon = {
            add: function(l) {
                var pList = PointList.add(l, true).v;
                return new OpenLayers.Geometry.Polygon(LineRing.add(l));
            },
            addFV: function(l, style, cb){
                var fv = new OpenLayers.Feature.Vector(this.add(l), null, style );
                fv.style = style;
                fv.attributes = {'devId' : l[0][2], 'chan': l[0][3] };
                if (typeof cb != 'undefined') cb(fv);
                return fv;
            },
            createLV: function(id, l, style, cb) {
                var v = new OpenLayers.Layer.Vector(id, { rendererOptions: {zIndexing: true} });
                if (typeof l != 'undefined') v.addFeatures([this.addFV(l, style, cb)]);
                return v;
            }
        };

        var Circle = {
            add: function(origin, radius) {
                origin = new OpenLayers.Geometry.Point(origin[0], origin[1]);
                var ret = new OpenLayers.Geometry.Polygon.createRegularPolygon(origin, radius, 40);
                return ret;
            },
            addFV: function(origin, radius, style) {
                var geoFeat = new OpenLayers.Feature.Vector(this.add(origin, radius), null, style);

                var devId = origin[2];
                var chanId = origin[3];
                var Pwr = origin[4];
                geoFeat.attributes = {'devId' : devId, 'chan': chanId, 'Pwr': Pwr };
                return geoFeat;
            }
        };

        return { 
            Point: Point, 
            PointList: PointList, 
            LineString: LineString,
            LineRing: LineRing,
            Markers: Markers,
            Polygon: Polygon,
            Circle: Circle
        };
    },
    MarkersDepr: function() {
        return {
            gen: function(data, style) {
                var pointList = [];
                for (var i=0; i<data.length; i++) {
                    var feature = new OpenLayers.Feature.Vector(
                        new OpenLayers.Geometry.Point(data[i][0], data[i][1], style)
                    );
                    feature.style = style;
                    feature.attributes = {'devId' : data[i][2] };
                    pointList.push(feature);
                }
                return pointList;
            }
        }
    },
    CALC : {
        getByPwr: function(Pwr) {
            //var idx = Pwr > -10 ? (Pwr>0 ? 1 : 2) : 400;
            var idx = Pwr > 18 ? (Pwr>22 ? 1 : 2) : 400;
            var stl = {1: 'red', 2: 'blue', 400: 'green'};

            return [0.088/idx, STYLES[stl[idx]]]; // 0.088 corresponds to 20,000 meters on the map
        }
    },
    visCount : {'red': 0, 'green': 0, 'blue': 0},
    FV: function(inF) {
        ret = {
            F: null,
            add: function(data, style, cb) {
                return UT.Shape()[inF].addFV(data, style);
            }, 
            showDev: function(devId, doShow) {
                var feature = inF.getFeaturesByAttribute('devId', parseInt(devId))[0];
                if (typeof feature != 'undefined') 
                    if (doShow) {
                        feature.style = STYLES.mark24red;
                        setTimeout(function() { feature.style = STYLES.mark24green; }, 0);
                    }  else {
                        feature.style = STYLES.noshow;
                    }
            },
            addCont: function(orig, hidden) {
                var params = UT.CALC.getByPwr(orig[4]);
                UT.visCount[params[1].id]++;
                if ( hidden ) {
                    var cont = UT.Shape()['Circle'].addFV(orig, params[0], STYLES.noshow);
                    var mark = UT.FV('Markers').add([orig], STYLES.noshow)[0];
                } else {
                    var cont = UT.Shape()['Circle'].addFV(orig, params[0], params[1]);
                    var mark = UT.FV('Markers').add([orig], STYLES.markPin24)[0];
                }
                var hash = orig[2] + '_' + orig[3];
                cont.attributes = 
                mark.attributes = {
                    'devId' : orig[2], 
                    'chan':   orig[3], 
                    'Pwr':    orig[4], 
                    'hash':   hash,
                    'range':  params[1].id 
                };
                this.F = [cont, mark];

                return this;
            },
            showCont: function(hash, doShow, ranges) {
                var Fs = inF.getFeaturesByAttribute('hash', hash);
                var range = false;
                for (var i=0; i<Fs.length; i++) {
                    range = Fs[i].attributes.range;
                    Fs[i].style = STYLES[doShow && ranges[range] ? Fs[i].attributes.range : 'noshow'];
                }
            },
            showRange: function(stlId, doShow) {
                //if (inF.attributes.range == stlId && inF.style.id != 'noshow')
                if (inF.attributes.range == stlId)
                    //inF.style.display = doShow ? 'block' : 'none';
                    inF.style = STYLES[doShow ? inF.attributes.range : 'noshow'];
            },
        }
        return ret;
    },
    // We don't allow for layers with the same id/name when inLayer is passed as string
    LV: function(inLayer, create, add) {
        if (typeof inLayer == 'string' && inLayer) {
            var searchLayers = GLOB.mapPanel.map.getLayersByName(inLayer);
            if (searchLayers.length > 0 ) {
                inLayer = searchLayers[0];
            } else if (typeof create != 'undefined' && create) {
                inLayer = UT.LV().createL(inLayer).L;
                if (typeof add != 'undefined' && add) GLOB.map.addLayer(inLayer);
            }   // else inLayers maintains its argument value of type string as passed to the function
        }       // it will be used by createL for adding features
        
        return {
            L: inLayer,
            createL: function(id, style, data) {
                this.L       = new OpenLayers.Layer.Vector(id, id, { rendererOptions: {zIndexing: true} });
                this.L.style = (typeof style != 'undefined' && style) ? style : null;

                if (typeof inLayer != 'undefined') 
                    this.L.addFeatures(UT.Shape()[inLayer].addFV(id, data, style));
                return this;
            },
            attrs: function(attrs) {
                for (var i in attrs) this.L[i] = attrs[i];
            },
            addF : function(features) {
                this.L.addFeatures(features);
                return this;
            },
            addMarkers: function(data, style) {
               inLayer.addFeatures(UT.FV('Markers').add(data, style));
               return this;
            },
            clear: function() {
                for (var i=0; i<inLayer.features.length; i++)
                    inLayer.removeFeatures(inLayer.features[i]);
                return this;
            }
        };
    },
    ContourDepr: function(inLayer) {
        var ret = {
            find: function(hashId) {
                
            }
        }
        return ret;
    },
    MAP: {
        getLayersAttr: function(attrName) {
            var res = [];
            for (var i=0; i<GLOB.map.layers.length; i++){
                res.push(GLOB.map.layers[i][attrName]);
            }
            return res;
        },
        getIndices: function() {
            var res = [];
            for (var i=0; i<GLOB.map.layers.length; i++){
                res.push(GLOB.map.layers[i].name + ":" + GLOB.map.getLayerIndex(GLOB.map.layers[i]) + '  ');
            }
            return res;
        }
    },
    Html: function(targetPaneId, clearTree) {
        var targetPane  = document.getElementById(targetPaneId)
        clearTree = (typeof clearTree != 'undefined' && clearTree);
        if (clearTree)  while (targetPane.hasChildNodes()) targetPane.removeChild(targetPane.lastChild);

        var ret = {
            E: null,
            addDivToPane: function(divId, html, reuse) {
                reuse = (typeof reuse != 'undefined' && reuse);

                var el = document.getElementById(divId);
                if (!reuse || el == null) {
                    var el = document.createElement('div');
                    el.id = divId;
                    targetPane.appendChild(el);
                }
                ret.E = el;
                el.innerHTML = html;
                return ret;
            }, 
            remove: function(divId) {
                var el = document.getElementById(divId);
                if (el != null) el.parentNode.removeChild(el);
                return ret;
            },
            // ['front', 'back' , 'noshow'];
            // ['Show All', 'Bring To Front', 'Hide All'];
            rotate: function(el, rotA,  cb) {
                var currentAttr = el.getAttribute('showAll');

                var idx = 0;
                while (idx < rotA[0].length && rotA[0][idx] != currentAttr) idx++;
                if (++idx == rotA[0].length) idx = 0;

                el.setAttribute('showAll', rotA[0][idx]);
                el.innerText = rotA[1][idx];

                if (typeof cb == 'function') cb(idx);
            }
        }

        return ret;
    },

    notify : function(divId, delayMultiplier) {
        delayMultiplier = typeof delayMultiplier == 'undefined' ? 1 : delayMultiplier;

        var notifyDelay = config.notify.delay * delayMultiplier;;
        if (divId == 'clear') {
            $('#topNotify >*').slideUp();
            setTimeout( function()  { $('#topNotify >*').remove(); }, 400);
            return null;
        }

        if (typeof divId == 'string' && divId.length) {
            if ($('#topNotify #' + divId).length == 0)
                $('#topNotify').append('<div id="' + divId + '"></div>');
        } else return null;

        var infoDiv = $('#topNotify #' + divId);

        return {
            cancel: function() {
                infoDiv.remove();
            },
            set: function(html, infoType) {
                if (typeof infoType == 'undefined') infoType = 'info';

                infoDiv.attr('class', infoType).html(html).slideDown();
                setTimeout( function() { infoDiv.slideUp() }, notifyDelay); 
            }
        } 
    },
    js: {
        hasVal: function(arr, val) {
            val = typeof val != 'undefined' ? val : true;
            var ret = false;
            for (var i=0 in arr) {
                if (arr[i] === val) {
                    ret = true;
                    break;
                }
            }
            return ret;
        }
    },
    ctrl: function(ctrlType) {
        var ml = new OpenLayers.Layer.Vector("WhiteSpace Selects");
        var pointControl =  new OpenLayers.Control.DrawFeature(
            ml, OpenLayers.Handler.Point, { callbacks: 
                { 
                    done: function(a) {
                        a.lon = a.x;
                        a.lat = a.y;
                        SELCallbacks.point(a);
                        pointControl.deactivate();
                    }
                }
            }
        );
        var pointAction= new GeoExt.Action({
            text: "Injected Channels at Point",
            map: map,
            toggleGroup: "draw",
            allowDepress: false,
            group: "draw",
            //pressed: true,
            iconCls: "point menuAction",
            control: pointControl,
            tooltip: 'Point ctrl tip'
        });

        var pointCtrl = function(cb) {
            LG( 'pointCtrl in UT', SEL().inject(cb) );

        }
       
        return {
            point: pointCtrl
            
        };
    }
};


// Setup/Init

// Selectors
var selLayers = [];
window.SEL = function(plugin) {
    var map = (typeof plugin == 'undefined') ? GLOB.map : plugin.map;

    var toolbarItems = [], action, actions = {};
    var ml = new OpenLayers.Layer.Vector("Selects");

    var init = function() { 
        return toolbarItems;
    }

    var initPlug = function() {
        pointAction.show();
        boxAction.show();
        pointControl.activate();
    }

    var setCallbacks = function(callbacks) { CBs = callbacks; }

    var boxControl = new OpenLayers.Control.DrawFeature(
        ml, OpenLayers.Handler.Box, { callbacks: { done: function(a) {
        SELCallbacks.box(a);
        boxControl.deactivate();
    }}});
    
    var centerControl = new OpenLayers.Control.DrawFeature(
        ml, OpenLayers.Handler.Point, { callbacks: { done: function(a) {
            a.lon = a.x;
            a.lat = a.y;
            map.setCenter(a);
            centerControl.deactivate();
        } } }
    );

    var pointControl =  new OpenLayers.Control.DrawFeature(
            ml, OpenLayers.Handler.Point, { callbacks: 
            { 
                done: function(a) {
                    a.lon = a.x;
                    a.lat = a.y;
                    SELCallbacks.point(a);
                    pointControl.deactivate();
                }
            }
        }
    );
    
    var pointAction= new GeoExt.Action({
        text: "Channels at Point",
        map: map,
        toggleGroup: "draw",
        allowDepress: false,
        group: "draw",
        //pressed: true,
        iconCls: "point menuAction",
        control: pointControl,
        tooltip: 'Point ctrl tip'
    });

    toolbarItems.push(pointAction);
    toolbarItems.push("-");

    boxAction = new GeoExt.Action({
        text: "Devices in Area",
        map: map,
        toggleGroup: "draw",
        allowDepress: false,
        group: "draw",
        checked: false,
        pressed: false,
        iconCls: "box menuAction",
        control: boxControl
    });
    var areaSel = action;

    toolbarItems.push(boxAction);
    toolbarItems.push("-");

    action = new GeoExt.Action({
        text: "Center Map",
        map: map,
        toggleGroup: "draw",
        allowDepress: false,
        pressed: false,
        checked: false,
        group: "draw",
        iconCls: "centerAction",
        control: centerControl
    });
    toolbarItems.push(action);
    toolbarItems.push("-");

    var button = new OpenLayers.Control.Button({
        displayClass: "myButton", 
        trigger: function(e) { alert('button' ); },
        callbacks: { done: function(e) { alert('done'); } },
        text: 'Whitespace'
    });
    var buttonAction = new GeoExt.Action({
        text: "Whitespace Button",
        map: map,
        toggleGroup: "draw",
        allowDepress: false,
        pressed: false,
        checked: false,
        group: "draw",
        iconCls: "centerAction",
        control: button
    });

    //toolbarItems.push(action);
    toolbarItems.push("->");


    pointAction.hide();
    boxAction.hide();

    action = new GeoExt.Action({
        text: "Whitespace",
        control: new OpenLayers.Control.SelectFeature(GLOB.map.layers[0], {
            hover: true,
            type: OpenLayers.Control.TYPE_TOGGLE,
            callbacks: { done: function(e) { alert('done'); } }
        }),
        map: map,
        enableToggle: true,
        tooltip: "Whitespace Tool"
    });
    //actions["select"] = action;
    //toolbarItems.push(action);
    //toolbarItems.push("-");
    //toolbarItems.push(action);
    //toolbarItems.push("-");

    var menu = {
        text: "Available Plugins",
        menu: new Ext.menu.Menu({
            items: [
               //new Ext.menu.CheckItem(action)
               //,new Ext.menu.CheckItem(areaSel)
               //button
            ]
        })
    };
    //toolbarItems.push(menu);
    var inject = function(arg) {
        var tbar  = GLOB.mapPanel.getTopToolbar();
        tbar.addButton(buttonAction);
        return arg;
    };

    var remove = function(arg) {
        LG('call remove');
    };

    return {
        init: init,
        initPlug: initPlug,
        inject: inject,
        remove: remove,
        setCallbacks: setCallbacks
    }
}

window.SELCallbacks = { 
    box:    function() {
        UT.NG('ngnotif').callFn('addMsg', ['You must be logged in', 'warn', 2]).$digest();
    },
    point:  function() {
        UT.NG('ngnotif').callFn('addMsg', ['You must be logged in', 'warn', 2]).$digest();
    }
    //,tbar: SEL().init()
};

STYLES = {};  // STYLE Cache
STYLE = function(styleId, base) {
    var ret = {
       S: STYLES[styleId],
       init: function() {
           STYLES[styleId] = OpenLayers.Feature.Vector.style['default'];
           if (typeof STYLES[base] != 'undefined')
               STYLES[styleId] = angular.copy(STYLES[base]);
           STYLES[styleId].id = styleId;

           this.S = STYLES[styleId];

           return this;
       },
       add:  function(param, val)    { 
           if (typeof val == 'undefined')  for (var i in param) STYLES[styleId][i] = param[i];
           else                            STYLES[id][param] = val; 
           return this;
       },
       get: function(item) { 
           return (typeof item != 'undefined') ? STYLES['item'] : null;
       },
        rotate: function(style, states, cb) {
            switch(style) {
                case states[0]:
                    break;
                case states[1]:
                    break;
                case states[2]:
                    break;
            }
        
        }
    };

    if (typeof STYLES[styleId] == 'undefined') STYLES[styleId] = ret.init().S;
    return ret;
}

// Define styles here
var imgPath = UT.Host + 'Content/image/';

STYLE('base').add({graphicOpacity: 1, 'strokeWidth' : 1});
STYLE('green',  'base' ).add({'fillOpacity': 0.2, 'strokeColor': '#008800', 'fillColor' : '#44ffaa', externalGraphic : imgPath + 'pin.png', graphicWidth : 12,  graphicHeight: 24,  graphicYOffset: -24, display : 'block'});
STYLE('red',    'green').add({'strokeColor': 'red', 'fillColor' : '#ff8844'});
STYLE('blue',   'green').add({'strokeColor': '#0044ff', 'fillColor' : '#88aaff'});
STYLE('noshow',   'green').add({'strokeColor': 'transparent', 'fillColor' : 'transparent', 'display' : 'none'});

STYLE('greenL',   'green').add({'strokeColor': '#00aa00', 'fillOpacity' : 0.1 });
STYLE('greenM',   'green').add({});
STYLE('greenD',   'green').add({'strokeColor': '#002200', 'fillOpacity' : 0.6 });

var markerWidth24 = 16;
var markerWidth36 = 24;

STYLE('marker').add({   graphicHeight : 20, graphicYOffset : -20, 
                        graphicWidth : 12,  graphicXOffset : -6, 
                        externalGraphic : imgPath + 'marker.png', fillOpacity:1} );

STYLE('markDish', 'marker').add({ graphicWidth : 24,    graphicXOffset: -12, 
                                                        externalGraphic : imgPath + 'dish.png'});

STYLE('markPin36', 'marker').add({ graphicWidth : 24,  graphicHeight: 36, 
                                   graphicYOffset: -36,  graphicXOffset: -12,
                                   externalGraphic : imgPath + 'pin.png'})

STYLE('markTarget', 'markPin36').add({externalGraphic : UT.Host + "Content/image/target.png"});

STYLE('mark36blue', 'markPin36').add({ externalGraphic : imgPath + 'markblue.png'})
STYLE('mark36red',  'markPin36').add({ externalGraphic : imgPath + 'markred.png'})
STYLE('mark36green', 'markPin36').add({ externalGraphic : imgPath + 'markgreen.png'})

STYLE('markPin24', 'markPin36').add({ graphicWidth : markerWidth24, graphicXOffset: -(markerWidth24)/2, graphicYOffset: -24, graphicHeight: 24, externalGraphic : imgPath + 'pin.png'});

STYLE('mark24green', 'markPin24').add({ externalGraphic : imgPath + 'markgreen.png'})
STYLE('mark24red' , 'markPin24').add({ externalGraphic : imgPath + 'markred.png'})
STYLE('mark24blue', 'markPin24').add({ externalGraphic : imgPath + 'markblue.png'})
})();

