﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="Cloud1" generation="1" functional="0" release="0" Id="f6a0035e-097f-481c-802e-18b2a3d87a1c" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="Cloud1Group" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="BlobRole:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/Cloud1/Cloud1Group/LB:BlobRole:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="BlobRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Cloud1/Cloud1Group/MapBlobRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="BlobRole:StorageConnString" defaultValue="">
          <maps>
            <mapMoniker name="/Cloud1/Cloud1Group/MapBlobRole:StorageConnString" />
          </maps>
        </aCS>
        <aCS name="BlobRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/Cloud1/Cloud1Group/MapBlobRoleInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:BlobRole:Endpoint1">
          <toPorts>
            <inPortMoniker name="/Cloud1/Cloud1Group/BlobRole/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapBlobRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Cloud1/Cloud1Group/BlobRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapBlobRole:StorageConnString" kind="Identity">
          <setting>
            <aCSMoniker name="/Cloud1/Cloud1Group/BlobRole/StorageConnString" />
          </setting>
        </map>
        <map name="MapBlobRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/Cloud1/Cloud1Group/BlobRoleInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="BlobRole" generation="1" functional="0" release="0" software="C:\devC\WsdAlone\Cloud1\csx\Debug\roles\BlobRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="StorageConnString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;BlobRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;BlobRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/Cloud1/Cloud1Group/BlobRoleInstances" />
            <sCSPolicyUpdateDomainMoniker name="/Cloud1/Cloud1Group/BlobRoleUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/Cloud1/Cloud1Group/BlobRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="BlobRoleUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="BlobRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="BlobRoleInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="1f54e237-5a9e-4795-8108-8e52de535749" ref="Microsoft.RedDog.Contract\ServiceContract\Cloud1Contract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="3a3f69ff-4c06-42f8-9273-17e66ba5efe2" ref="Microsoft.RedDog.Contract\Interface\BlobRole:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/Cloud1/Cloud1Group/BlobRole:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>