(function() {
    if (typeof window.ICSPlugins == 'undefined') window.ICSPlugins = {};

    var selCtrl;
    var lastLoc;
    var coverageLO = {};
    var deviceLO = {};
    var _this = null;
    var layerIdx   = 100;
    var featureIdx = 100;

    window.ICSPlugins.WSD = _this = { 
        ranges : {'red': true, 'green': true, 'blue': true},
        init: function() {
            window.MapMenu.initPlug();
            coverageLO = UT.LV('Device Coverage', 'create', 'add');
            deviceLO = UT.LV('Selected Devices', 'create', 'add');
            selCtrl = new OpenLayers.Control.SelectFeature([deviceLO.L, coverageLO.L]);
            GLOB.map.addControl(selCtrl);
            selCtrl.activate();
            lastLoc = null;
            UT.ctrl().point( function() {LG("cb called "); });
        },
        map: map,
        callbacks : {
            point: function(p) {
                //lastLoc = map.getLonLatFromPixel(new OpenLayers.Pixel(p.layerX, p.layerY));
                lastLoc = p;
                UT.NG('ngnotif').callFn('addMsg', ['Loading Channels for location ('
                    + p.lon.toFixed(4) + ', ' 
                    + p.lat.toFixed(4) + ') ...please wait', 'info', 2]).$digest();
                UT.Ajax().get('WsTvwsLookup', _this.methods.chanButtons, p);
                UT.NG().update('ngchannellist', 'chanId', 0);
            },
            box: function(p) {
                if (typeof p.left != 'undefined') {
                    var p1 = map.getLonLatFromPixel(new OpenLayers.Pixel(p.left, p.bottom));
                    var p2 = map.getLonLatFromPixel(new OpenLayers.Pixel(p.right, p.top));

                    UT.Ajax().get('WsDeviceList', _this.methods.devButtons,
                        {lon: p1.lon, lat: p1.lat}, {lon: p2.lon, lat:p2.lat}
                    );

                    if ( deviceLO.L.events.listeners.featureselected.length == 0 ) {
                        deviceLO.L.events.on({ featureselected: _this.handlers.deviceSelected})
                    }
                    UT.NG().update('ngdevicelist', 'devId', 0);
                    UT.NG('ngnotif').callFn('addMsg', ['Loading devices, please wait', 'info', 2]).$digest();
                }
            }
        },
        handlers: {
            chanSelected: function(e) {
                GLOB.map.setLayerIndex(e.feature.layer, layerIdx++);
                /*
                LG(e.feature.attributes.chan, ' feature clicked chan');
                UT.NG('ngdevicelist').callFn('toFront', 'dev').$digest();
                UT.NG('ngchannellist').callFn('toFront', 'dev').$digest();
                */
                _this.methods.chanClick(e.feature.attributes.chan, true);
            },
            deviceSelected: function(e) {
                GLOB.map.setLayerIndex(e.feature.layer, layerIdx++);
                UT.NG('ngchannellist').callFn('toFront', 'chan').$digest();
                UT.NG('ngdevicelist').callFn('toFront', 'chan').$digest();
            }
        },
        methods: {
            redrawLayer: function(which) { 
                (which == 'dev' ? deviceLO : coverageLO).L.redraw(); 
                GLOB.map.setLayerIndex((which == 'dev' ? deviceLO : coverageLO).L, layerIdx++); 
            },
            toFront:     function(which) { 
                GLOB.map.setLayerIndex((which == 'chan' ? deviceLO : coverageLO).L, layerIdx++); 
            },
            devClick: function(devId, doShow) { 
                UT.FV(deviceLO.L).showDev(devId, doShow); 
            },
            channelReverse: function(btnList) {
                for (var i in btnList) 
                    _this.methods.chanClick(i, null, btnList[i]);
            },
            chanClick: function (chanId, doShow) {
               var devs = UT.Stor('Channels', 'Devices').get(chanId);
               if (devs)
                   for (var i=0; i<devs.length; i++) {
                       UT.FV(coverageLO.L).showCont(devs[i].Id + '_' + chanId, doShow, _this.ranges);
                   }
            },
            showRange: function(which, doShow, vis) {
                var stlId;
                switch (which) {
                    case 'short' : stlId = 'green'; break;
                    case 'med'   : stlId = 'blue'; break;
                    case 'long'  : stlId = 'red'; break;
                }

                _this.ranges[stlId] = doShow;

                for (var i=0; i<coverageLO.L.features.length; i++)  {
                    if (vis[coverageLO.L.features[i].attributes.chan]) {
                        UT.FV(coverageLO.L.features[i]).showRange(stlId, doShow);
                    }
                }

                coverageLO.L.redraw();
            },
            chanButtons: function (res) {
                if (res.length > 0 ) {
                    UT.NG('ngnotif').callFn('addMsg', ["Loaded " + res.length + " Channels", 'success', 2]).$digest();

                    var chanCount, chan;
                    var tplData = {'channels': [],
                        'lon' : lastLoc.lon.toFixed(4),
                        'lat' : lastLoc.lat.toFixed(4)
                    };
                    for (var i=0 ; i<res.length; i++) {
                        chanCount = UT.Stor('Channels', 'Devices').get(res[i].Channel);
                        res[i].dev = chanCount;
                        chan = UT.Stor('Devices').getChannels(res[i].Channel);
                        res[i].chanCount = chanCount == null ? 0 : chanCount.length;
                        if (chan != null) {
                            res[i].Pwr = chan[0] != null ? chan[0].Pwr : -7;
                        }

                        tplData.channels.push(res[i]);
                    }

                    _this.methods.genContours(tplData, true);
                    UT.NG('westPane').inject('ngchannellist', tplData);
                }
            },
            genContours: function(coverageData, hidden) {
                if ( coverageLO.L.features.length == 0 ) {
                    var origin = [], featuresA = [], chan;

                    for (var i=0; i<coverageData.channels.length; i++) {
                        chan= coverageData.channels[i];
                        if (chan.dev != null )
                            for (var j=0; j<chan.dev.length; j++) {
                                var chanDev = chan.dev[j];
                                origin = [chanDev.lon, chanDev.lat, chanDev.Id,  chan.Channel, chanDev.Pwr];
                                featuresA = featuresA.concat(UT.FV().addCont(origin, hidden).F);
                            }
                    }

                    coverageLO.addF(featuresA).L.events.on({featureselected: _this.handlers.chanSelected});
                }
            },
            devButtons: function(res) {
                if (res.length > 0) {
                    UT.NG('ngnotif').callFn('addMsg', ["Loaded " + res.length + " Devices", 'success', 2]);

                    var tplData = {'devices' : []};
                    for (var i=0; i< res.length; i++)
                        tplData.devices.push({'devId' : res[i][2], 'devClass' : 'blue'});

                    deviceLO.L.removeAllFeatures();
                    UT.LV(deviceLO.L).addMarkers(res, STYLES.noshow);
                    UT.NG('eastPane').inject('ngdevicelist', tplData);
                }
            },
            redrawCoverage: function() { coverageLO.L.redraw(); }
        },
        ctrl: {
        },
        treeDepr: {
            init: function(mapPanel) {
                UT.Html('westPane').addDivToPane('devices','');
                new Ext.tree.TreePanel({
                    root:
                        new GeoExt.tree.LayerContainer({ 
                            text: "Channel Coverage Layers",
                            layerStore: mapPanel.layers, 
                            expanded: true ,
                            leaf:true,
                            loader: { 
                                filter: function(record) {
                                    return true;
                                    //return record.get('layer').name.indexOf('Channel ') > -1;
                                }
                            }
                        }),
                    renderTo: 'devices'
                })
            }
        }
    }
})();

