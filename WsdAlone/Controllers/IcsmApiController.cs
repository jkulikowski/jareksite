﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using svc = WsdAlone.MdnSvc;
using models = WsdAlone.Models;
using WsdAlone.Models;

namespace ICSWebPTS.Controllers
{

    public class IcsmApiController : Controller
    {
        public static svc.MdnSrvClient client = new svc.MdnSrvClient();

        /*
         *  Private Utility methods - START
         */
        private string GetSessToken()
        {
            return Session["MdnTokenId"].ToString();
        }
        public string Login(string username, string passwd)
        {
            models.Auth au = new models.Auth();
            return au.MdnLogin(username, passwd);
        }

        private string toJson(object obj)
        {
            System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            oSerializer.MaxJsonLength = Int32.MaxValue;
            return oSerializer.Serialize(obj);
        }

        private T fromJson<T>(string postData)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(postData));
            return (T)ser.ReadObject(stream);
        }

        private string retJson(string status, string message)
        {
            return "{\"success\" : " + status + ", \"message\" : \"" + message + "\"}";
        }
        /*
         *  Private Utility methods - END
         */

        public string LockRecord(string table, string code)
        {
            bool ret = client.LockRecord(GetSessToken(), table, code);
            return ret ? "True" : "False";
        }

        public string UnlockRecord(string table, string code)
        {
            client.UnlockRecord(GetSessToken(), table, code);
            return "Ok";
        }
        public string GetAllSelects(string selType = "", string itemType = "")
        {
            string ret = "";
            List<string> types = new List<string>();

            // Equipment
            if (selType == "Equipment" || selType == "")
            {
                types.Add("eri_EqpType");
                types.Add("eri_EQUIP_ANALOG_DIGITAL");
                types.Add("eri_EquipSensitUnit");
                types.Add("eri_FixMobTypeOfPower");
            }

            // Antenna
            if (selType == "Antenna" || selType == "")
            {
                types.Add("eri_ANT_CLASS2");
                types.Add("eri_ANT_GAIN_TYPE");
            }

            // Site
            if (selType == "Site" || selType == "")
            {
                types.Add("eri_COUNTRY");
                types.Add("lov_PROVINCES");
                types.Add("lov_STATION_TOWER");
            }

            // Microwave
            //types.Add("");

            // Other Terrestrial
            //types.Add("");

            Dictionary<string, List<ComboClass>> all = new Dictionary<string, List<ComboClass>>();
            foreach (string type in types)
            {
                if (itemType == "" || itemType == type)
                {
                    all[type] = new List<ComboClass>();
                    svc.LovItem[] ll = client.GetListOfValues(type, "ENG");
                    foreach (svc.LovItem litm in ll)
                    {
                        ComboClass cbCls = new ComboClass();
                        cbCls.val = litm.Code;
                        cbCls.label = litm.Description;
                        all[type].Add(cbCls);
                    }
                }
            }

            return toJson(all);
        }
        /*
         * DETAIL GETTERS - START   
         */
        public string GetEquipment(string id)
        {
            return toJson(client.GetEquipmentDetails(GetSessToken(), id.ToString()));
        }
        public string GetAntenna(string id)
        {
            return toJson(client.GetAntennaDetails(GetSessToken(), id.ToString()));
        }
        public string GetSite(string id)
        {
            return toJson(client.GetPositionDetails(GetSessToken(), id));
        }
        public string GetMicrowave(string id)
        {
            return toJson(client.GetPositionDetails(GetSessToken(), id));
        }
        public string GetOtherTer(int id)
        {
            return toJson(client.GetOtherTerrestrialDetails(GetSessToken(), id));
        }
        /*
         * DETAIL GETTERS - END  
         */

        /*
         * DELETEs - START
         */
        public string DelEquipment(string id)
        {
            try { client.DeleteEquipment(GetSessToken(), id); return retJson("true", "Item Deleted"); }
            catch (Exception e) { return retJson("false", e.Message); }
        }
        public string DelAntenna(string id)
        {
            try { client.DeleteAntenna(GetSessToken(), id); return retJson("true", "Item Deleted"); }
            catch (Exception e) { return retJson("false", e.Message); }
        }
        public string DelSite(string id)
        {
            try { client.DeletePosition(GetSessToken(), id); return retJson("true", "Item Deleted"); }
            catch (Exception e) { return retJson("false", e.Message); }
        }
        public string DelMicrowave(int id)
        {
            return retJson("false", "Not implemented yet.");
        }
        public string DelOtherTer(int id)
        {
            try { client.DeleteOtherTerrestrial(GetSessToken(), id); return retJson("true", "Item Deleted"); }
            catch (Exception e) { return retJson("false", e.Message); }
        }
        /*
         * DELETEs - END
         */

        [HttpPost]
        public string UpdEquipment(string postData)
        {
            string res = "";

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(svc.EquipmentDetails));
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(postData));
            svc.EquipmentDetails obj = (svc.EquipmentDetails)ser.ReadObject(stream);

            if (obj.Code != "-1")
            {
                client.UpdateExistingEquipment(GetSessToken(), obj);
            }
            else
            {
                client.CreateNewEquipment(GetSessToken(), obj);
            }

            string msg = "Equipment with id:" + obj.Code + " was created/updated successfully";
            res = "{\"message\":\"" + msg + "\", \"success\":true}";

            return res;
        }

        [HttpPost]
        public string UpdAntenna(string postData)
        {
            string res = "";

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(svc.AntennaDetails));
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(postData));
            svc.AntennaDetails obj = (svc.AntennaDetails)ser.ReadObject(stream);

            if (obj.Code != "-1")
            {
                client.UpdateExistingAntenna(GetSessToken(), obj);
            }
            else
            {
                client.CreateNewAntenna(GetSessToken(), obj);
            }

            string msg = "Antenna with id:" + obj.Code + " was created/updated successfully";
            res = "{\"message\":\"" + msg + "\", \"success\":true}";

            return res;
        }

        [HttpPost]
        public string UpdSite(string postData)
        {
            string res = "";

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(svc.PositionDetails));
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(postData));
            svc.PositionDetails obj = (svc.PositionDetails)ser.ReadObject(stream);

            if (obj.Code != "-1")
            {
                client.UpdateExistingPosition(GetSessToken(), obj);
            }
            else
            {
                client.CreateNewPosition(GetSessToken(), obj);
            }

            string msg = "Site with id:" + obj.Code + " was created/updated successfully";
            res = "{\"message\":\"" + msg + "\", \"success\":true}";

            return res;
        }

        [HttpPost]
        public string UpdOtherTer(string postData)
        {
            string res = "";

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(svc.OtherTerrestrialDetails));
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(postData));
            svc.OtherTerrestrialDetails obj = (svc.OtherTerrestrialDetails)ser.ReadObject(stream);

            if (obj.ID > -1)
            {
                client.UpdateExistingOtherTerrestrial(GetSessToken(), obj);
            }
            else
            {
                client.CreateNewOtherTerrestrial(GetSessToken(), obj);
            }

            string msg = "Other Terrestrial station with id:" + obj.ID.ToString() + " was updated successfully";
            res = "{\"message\":\"" + msg + "\", \"success\":true}";

            return res;
        }



        public string ListOtherTer(int page = 1, int start = 1, int limit = 10,
            int minId = 1, int maxId = 0x7FFFFFFF,
            string name = "*", string category = "*", string cls = "*", string desigEmission = "*",
            double minPwrRad = 1e-99, double maxPwrRad = 1e-99,
            double TxFreq = 1e-99, double minTxFreq = 1e-99, double maxTxFreq = 1e-99,
            double RxFreq = 1e-99, double minRxFreq = 1e-99, double maxRxFreq = 1e-99,
            string equipManufacturer = "*", string equipModel = "*",
            string antManufacturer = "*", string antModel = "*",
            string posName = "*", string posCity = "*",
            double lonW = 1e-99, double latN = 1e-99, double lonE = 1e-99, double latS = 1e-99
        )
        {
            string res = "";
            NameValueCollection s = Request.QueryString;

            models.MdnData<svc.OtherTerrestrialSummary, svc.OtherTerrestrialDetails> res1
                = new models.MdnData<svc.OtherTerrestrialSummary, svc.OtherTerrestrialDetails>();

            if (GetSessToken() != null)
            {
                res1.list = client.GetOtherTerrestrialList(GetSessToken()
                        , minId, maxId //Pass 0x7FFFFFFF if not applicable;
                        , name, category, cls, desigEmission //pass "*" for no filtering
                        , minPwrRad, maxPwrRad //pass 1e-99 for no filtering
                        , TxFreq, minTxFreq, maxTxFreq
                        , RxFreq, minRxFreq, maxRxFreq
                        , equipManufacturer, equipModel
                        , antManufacturer, antModel
                        , posName, posCity
                        , lonW, latN, lonE, latS
                        , start - 1, limit
                    );


                res1.count = client.GetOtherTerrestrialCount(GetSessToken()
                     , minId, maxId
                     , name, category, cls, desigEmission //pass "*" for no filtering
                     , minPwrRad, maxPwrRad
                     , TxFreq, minTxFreq, maxTxFreq
                     , RxFreq, minRxFreq, maxRxFreq
                     , equipManufacturer, equipModel
                     , antManufacturer, antModel
                     , posName, posCity
                     , lonW, latN, lonE, latS
                 ).ToString();


                System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                oSerializer.MaxJsonLength = Int32.MaxValue;
                res = oSerializer.Serialize(res1);
            }
            else
            {
                res = retJson("false", "Session Expired");
            }

            return res;
        }

        /*
         * Other Terrestrial - END            
         */

        public string ListEquipment(int page = 1, int start = 1, int limit = 10,
            string manufacturer = "*", string model = "*", double freq = 1e-99)
        {
            string ret = "";
            string tok = GetSessToken();

            if (tok != "")
            {
                models.MdnData<svc.EquipmentSummary, int> res
                    = new models.MdnData<svc.EquipmentSummary, int>();

                res.list = client.GetEquipmentsList(tok, manufacturer, model, freq, start-1, limit);
                res.count = client.GetEquipmentsCount(tok, manufacturer, model, freq).ToString();
                ret = toJson(res);
            }
            else
            {
                ret = retJson("false", "Session Expired");
            }
            return ret;
        }

        public string ListAntenna(int page = 1, int start = 1, int limit = 10,
                string manufacturer = "*", string model = "*", double freq = 1e-99)
        {
            string ret = "";
            string tok = GetSessToken();
            if (tok != "")
            {
                models.MdnData<svc.AntennaDetails, int> res = new models.MdnData<svc.AntennaDetails, int>();

                res.count = client.GetAntennasCount(tok, manufacturer, model, freq).ToString();
                res.list = client.GetAntennasList(tok, manufacturer, model, freq, start-1, limit);

                ret = toJson(res);
            }
            else ret = retJson("false", "Session Expired");

            return ret;
        }

        public string ListSite(int page, int start, int limit,
            string Name = "*", string City = "*",
            double Lon = 1e-99, double Lat = 1e-99, double Radius = 1e-99
        )
        {
            //string pRes = "OK";
            string res = "";
            int skip = (page * limit) - limit;
            string nameFilter = "*";
            string cityFilter = "*";
            double lng = 0;
            double lat = 0;
            double rad = 0;
            double longitudeWDec = 1e-99;
            double latitudeNDec = 1e-99;
            double longitudeEDec = 1e-99;
            double latitudeSDec = 1e-99;

            //SiteJsonClass JSONListOfSites = new SiteJsonClass();
            models.MdnData<svc.PositionDetails, svc.PositionDetails> sites
                = new models.MdnData<svc.PositionDetails, svc.PositionDetails>();
            string tok = GetSessToken();

            if (tok != "")
            {
                // try  {
                /*
                string[] valTab = formContent.Split('&');
                for (int i = 0; i < valTab.Length; i++)
                {
                    string[] subTab = valTab[i].Split('=');
                    if (subTab[0] == "Name_siteSRC" && (subTab[1] != "" && subTab[1] != null))
                    {  
                        nameFilter = subTab[1];
                    }
                    if (subTab[0] == "City_siteSRC" && (subTab[1] != "" && subTab[1] != null))
                    {
                        cityFilter = subTab[1];
                    }
                    if (subTab[0] == "Longitude_siteSRC_hidden" && (subTab[1] != "" && subTab[1] != null))
                    {
                        //lng = Convert.ToDouble(Utilities.ConvDecToSexagDms(Convert.ToDouble(subTab[1].Replace(',', '.'), new CultureInfo("en-US"))));
                        lng = Convert.ToDouble(subTab[1].Replace(',', '.'), new CultureInfo("en-US"));
                    }
                    if (subTab[0] == "Latitude_siteSRC_hidden" && (subTab[1] != "" && subTab[1] != null))
                    {
                        //lat = Convert.ToDouble(Utilities.ConvDecToSexagDms(Convert.ToDouble(subTab[1].Replace(',', '.'), new CultureInfo("en-US"))));
                        lat = Convert.ToDouble(subTab[1].Replace(',', '.'), new CultureInfo("en-US"));
                    }
                    if (subTab[0] == "Radius_siteSRC" && (subTab[1] != "" && subTab[1] != null))
                    {
                        rad = Convert.ToDouble(subTab[1].Replace(',', '.'), new CultureInfo("en-US"));
                    }
                }
                if (lat != 0 && lng != 0 && rad != 0)
                {
                    Geolocation.CoordinateBoundaries boundaries = new Geolocation.CoordinateBoundaries(lat, lng, rad);

                    longitudeWDec = boundaries.MinLongitude;
                    latitudeNDec = boundaries.MaxLatitude;
                    longitudeEDec = boundaries.MaxLongitude;
                    latitudeSDec = boundaries.MinLatitude;
                }
                */
                double lonW = 1e-99;
                double latN = 1e-99;
                double lonE = 1e-99;
                double latS = 1e-99;

                sites.count = client.GetPositionsCount(tok,
                    Name, City, lonW, latN, lonE, latS).ToString();

                sites.list = client.GetPositionsList(tok,
                    Name, City, lonW, latN, lonE, latS, start-1, limit);

                System.Web.Script.Serialization.JavaScriptSerializer oSerializer
                    = new System.Web.Script.Serialization.JavaScriptSerializer();
                oSerializer.MaxJsonLength = Int32.MaxValue;
                res = oSerializer.Serialize(sites);
            }
            /*
           catch (Exception e)
           {
               string err = e.Message.ToString().Replace("'", "").Replace("{0}", "");
               res = "{'message':'" + err + "', 'success':false}";
           }*/
            //}
            else
            {
                res = "{'message':'User session expired', 'success':false}";
            }
            return res;
        }

    }
}
