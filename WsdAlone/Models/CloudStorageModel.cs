﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
//using Microsoft.ServiceBus;
//using Microsoft.ServiceBus.Messaging;

namespace WsdAlone.Models
{
    public class CloudStorageModel
    {
        public bool isAlreadyInit { get; set; }
        public string cloudAccountName { get; set; }
        public string cloudLongName { get; set; }
        public string cloudAccountKey { get; set; }
        public string cloudAccountSig { get; set; }
        public string cloudDocumentSig { get; set; }
        public string cloudMaxSize { get; set; }
        public float cloudPackageSize { get; set; }
        public string cloudX64Key { get; set; }
        public string cloudClientPlugins { get; set; }
        public DateTime cloudAccountSigExperity { get; set; }
        public bool isOnlyPublic { get; set; }
        public string cloudRootStorage { get; set; }
        //public QueueClient coverToGeoJsonQueue { get; set; }
    }
}