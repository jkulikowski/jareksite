﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;
using System.Security.Cryptography;
using WsdAlone.Models;
using WsdAlone.MdnSvc;

namespace WsdAlone.Controllers
{
    public class AuthController : ApiController
    {
        public static CloudStorageModel blobStorage { get; set; }
        public static ClientTableEntity clientTabEntity { get; set; }
        public static string ZipEntityJSON { get; set; }
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        public static string Encode(string original)
        {
            byte[] encodedBytes;

            using (var md5 = new MD5CryptoServiceProvider())
            {
                var originalBytes = Encoding.Default.GetBytes(original);
                encodedBytes = md5.ComputeHash(originalBytes);
            }

            return Convert.ToBase64String(encodedBytes);
        }

        public string Login(string user, string pass)
        {
            string qUser = Request.QueryString["user"];
            string qPasswd = Request.QueryString["pass"];
            if (qUser == null || qPasswd == null)
            {
                return "{success: false}";
            }
            else
            {
                blobStorage = new CloudStorageModel();
                CloudStorageModel blob = new CloudStorageModel();
                ServiceUtils svcU = new ServiceUtils();

                ConnectionObjEntity conn = svcU.AuthCheck(qUser.ToString(), qPasswd.ToString());

                if (conn.isValidateByWS)
                {

                    FormsAuthentication.SetAuthCookie(qUser.ToString(), true);
                /*
                    blobStorage = new CloudStorageModel();
                    blobStorage.isOnlyPublic = false;
                    blobStorage.isAlreadyInit = false;
                    blobStorage.cloudRootStorage = "root";
                    blobStorage.cloudAccountSigExperity = new DateTime();
                    blobStorage.cloudAccountKey = conn.icsOnlineClientKey;
                 */
                                      
                 ClientTableEntity clientAccount = ServiceUtils.GetCloudAccountIfExist(conn.icsOnlineClientKey);

                    Session["isAuth"] = true;
                    Session["conn"] = JsonConvert.SerializeObject(conn);
                    Session["acct"] = JsonConvert.SerializeObject(clientAccount);
                    Random rand = new Random();
                    string rnd = rand.Next().ToString() + DateTime.Now.ToString("HH:MM:ss.ffffff");
                    Session["key"] = Encode(rnd);
                   
                    Response.AppendHeader("conn", Session["conn"].ToString());
                    Response.AppendHeader("acct", Session["acct"].ToString());
                    Response.AppendHeader("apiKey", Session["key"].ToString());
                }
                else
                {
                    return "{success: false}";
                }
            }
            return "{success: true}";
        }

        public string Logout(){
       
            return "{success: true}";
        }

        public string MdnLogout()
        {
            Session["MdnTokenId"] = null;
            return "{\"message\": \"Session Ended\", \"success\" : true}";
        }

        public string MdnLogin(string username, string passwd)
        {
            MdnSrvClient srvClient = new MdnSrvClient();
            DateTime refTim =  srvClient.GetServerUtc();
              
            Session["MdnTokenId"] = null;
            
            string passTmHash = PasswordTimeHash(passwd, refTim);
             try
             {
                 UserProfile uws = srvClient.Login(username, refTim, passTmHash);
                 Session["MdnTokenId"] = uws.TokenId;
             }
             catch { }

             string res = "{\"success\": " + (Session["MdnTokenId"] == null ? "false" : "true") + "}";
             return res;
        }

         public string SHA256Hash(string input)
        {
            byte[] inputbytes = UTF8Encoding.UTF8.GetBytes(input);
            SHA256 sha = new SHA256CryptoServiceProvider();
            byte[] hash = sha.ComputeHash(inputbytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++) sb.Append(hash[i].ToString("X2"));
            return sb.ToString();
        }
         public string Stringify(DateTime tm)
        {
            string ret = ((((((tm.Year - 2000) * 12 + tm.Month) * 31 + tm.Day) * 24 + tm.Hour) * 60 + tm.Minute) * 60 + tm.Second).ToString();
            return ret;
        }
         public string PasswordTimeHash(string password, DateTime refTime)
        {
            string refTimeS = Stringify(refTime);
            string passHash = SHA256Hash(password);
            return SHA256Hash(refTimeS + passHash);
        }


    }
}