// Preload for testing
var allLonLat1 = {lon:50.04, lat:15.68};
var allLonLat2 = {lon:66.04, lat:29.68};
UT.Ajax().get('WsDeviceDetails', function(res) {}, allLonLat1, allLonLat2);

UT.Ajax().get('WsDeviceList', function(res) {}, allLonLat1, allLonLat2);
UT.Ajax().get('WsChannelsList', function(res) {} );

if (typeof GLOB == 'undefined' )
    GLOB = {
        map: null,
        page : null,
        mapPanel : null
    };

var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;
var proj1 = "EPSG:4326";
var map = new OpenLayers.Map({ 
    //projection: proj1,
    controls: [
                        new OpenLayers.Control.Navigation(),
                        new OpenLayers.Control.PanZoomBar(),
                        new OpenLayers.Control.LayerSwitcher({'ascending':false}),
                        //new OpenLayers.Control.Permalink(),
                        new OpenLayers.Control.ScaleLine(),
                        //new OpenLayers.Control.Permalink('permalink'),
                        //new OpenLayers.Control.MousePosition(),
                        //new OpenLayers.Control.OverviewMap(),
                        //new OpenLayers.Control.KeyboardDefaults()
    ]
    //,projection: new OpenLayers.Projection('EPSG:900913')
});

GLOB.map = map;
var openStreet = new OpenLayers.Layer.WMS("Open Street", "http://maps.opengeo.org/geowebcache/service/wms", {
    layers : "openstreetmap",
    format : "image/png"
});
map.addLayer(openStreet);

var globImg =  new OpenLayers.Layer.WMS(
    "Global Imagery", "http://maps.opengeo.org/geowebcache/service/wms", {layers: "bluemarble"}
);

globImg.isBase = true;
map.addLayer(globImg);

map.addLayer(new OpenLayers.Layer.WMS( "OpenLayers WMS", "http://vmap0.tiles.osgeo.org/wms/vmap0", {layers: 'basic'}));

if (false)
map.addLayer( new OpenLayers.Layer.Google(
    "Google Physical", {layers: "basic"}, {type: google.maps.MapTypeId.TERRAIN}
));

map.setCenter(UT.LonLat([57.02, 22.89]), 6);

function setTbar() {
    window.MapMenu = SEL({map: GLOB.map});
    return window.MapMenu.init();
}

Ext.onReady( function() {
    GLOB.mapPanel = new GeoExt.MapPanel({
        zoom:8,
        border: false,
        region: "center",
        map: map,
        //bbar: { height:30, contentEl: 'ngstatus' },
        tbar: setTbar()
        //tbar:  {height:80}
    });

    //GLOB.mapPanel.toolbars[1] = documen

    var sideDivsWidth = 180;

    GLOB.page = new Ext.Viewport({
        layout: "fit",
        hideBorders: true,
        items: {
            layout: "border",
            deferredRender: false,
            items: [
                GLOB.mapPanel,
                {   region:"north", width:100, contentEl: "topBar", height:64},
                {   region:"west", 
                    width:sideDivsWidth,
                    html : '<div id="westPane"></div>'
                    //,bbar: {height:130, html:'<div id="notify"></div>'},
                },
                {   region:"east", 
                    width:sideDivsWidth,
                    html: '<div id="eastPane"></div>'
                    //,bbar: { contentEl: "ngdevice", height:300 }
                },
                {   region:"south", 
                    height:30,
                    html: '<div id="southPane"></div>'
                }

            ]
        }
    });
    UT.NG('southPane').inject('ngnotif');
});

window.ActivePlugin = null;
function initPlug(currentPlug){
    "use strict";
    
    ActivePlugin = currentPlug;
    ActivePlugin.init();
    SELCallbacks = ActivePlugin.callbacks;
    
    if (false)
    UT.notify('intro', 5).set(
        '1. Click on the map to load the list channels available at the location of the click<br />' +
        '&nbsp;&nbsp;&nbsp;&nbsp;or<br />' +
        '2. Click on "Area Selector" button and select rectangular region on the map to load devices in the selcted region.' +
        '<br /><br />' + 
        '<div class="closeInfo">* Clicking anywhere on the black surface at any time closes this notification pane</div>', 'success'
        );
}

// unused but need to play with 
    function setGoogleMaps() {
        map = new OpenLayers.Map('map', {
            projection: 'EPSG:3857',
            layers: [
                new OpenLayers.Layer.Google(
                    "Google Physical",
                    {type: google.maps.MapTypeId.TERRAIN}
                ),
                new OpenLayers.Layer.Google(
                    "Google Streets", // the default
                    {numZoomLevels: 20}
                ),
                new OpenLayers.Layer.Google(
                    "Google Hybrid",
                    {type: google.maps.MapTypeId.HYBRID, numZoomLevels: 20}
                ),
                new OpenLayers.Layer.Google(
                    "Google Satellite",
                    {type: google.maps.MapTypeId.SATELLITE, numZoomLevels: 22}
                )
            ],
            center: new OpenLayers.LonLat(10.2, 48.9)
                // Google.v3 uses web mercator as projection, so we have to
                // transform our coordinates
    //            .transform('EPSG:4326', 'EPSG:3857')
    ,
            zoom: 5
        });
        map.addControl(new OpenLayers.Control.LayerSwitcher());
    }

