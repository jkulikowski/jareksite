﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.StorageClient;

namespace WsdAlone.Models
{
    public class ClientTableEntity : TableServiceEntity
    {
        public string ClientName { get; set; }
        public string ClientKey { get; set; }
        public string ClientLongName { get; set; }
        public string ClientAllowSize { get; set; }
        public string ClientX64Key { get; set; }
        public string ClientPlugins { get; set; }

        public ClientTableEntity() { }
    }
}