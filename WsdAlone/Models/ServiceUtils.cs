﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using Image2;
using System.IO;
using WsdAlone.WhiteSvc;
using WsdAlone.Models;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using System.Data;
using System.Data.Services.Client;

namespace WsdAlone.Models
{
    public class ServiceUtils
    {
        public static ClientTableEntity GetCloudAccountIfExist(string caName)
        {
            try
            {
                //CloudStorageModel csObj = (CloudStorageModel)HttpContext.Current.Session["blobStorage"];

                string connecString = Microsoft.WindowsAzure.CloudConfigurationManager.GetSetting("Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString");
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connecString);
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                TableServiceContext serviceContext = tableClient.GetDataServiceContext();
                ClientTableEntity clTabEnt = (from e in serviceContext.CreateQuery<ClientTableEntity>("ClientAccounts")
                                              where e.ClientName == caName
                                              select e).FirstOrDefault();

                HttpContext.Current.Session["clientTabEntity"] = clTabEnt;
                /*
                csObj.cloudAccountName = clTabEnt.ClientName;
                csObj.cloudLongName = clTabEnt.ClientLongName;
                csObj.cloudAccountKey = clTabEnt.ClientKey;
                csObj.cloudMaxSize = clTabEnt.ClientAllowSize;
                csObj.cloudX64Key = clTabEnt.ClientX64Key;
                csObj.cloudClientPlugins = clTabEnt.ClientPlugins;
                csObj.cloudRootStorage = "root";
                 */
                return clTabEnt;
            }
            catch
            {
                return null;
            }
        }
        public ConnectionObjEntity AuthCheck(string login = "auth", string psw = "secret")
        {
            ConnectionObjEntity res = new ConnectionObjEntity();
            string crmUrl = ConfigurationManager.AppSettings["CRMWebServiceURL"] + "?login={0}&psw={1}";
            string MD5Hash = EncodePassword(psw); string formatedUrl = String.Format(crmUrl, login, MD5Hash);

            WebRequest request = WebRequest.Create(formatedUrl);

            WebResponse response = request.GetResponse();
            using (var sr = new System.IO.StreamReader(response.GetResponseStream()))
            {
                XDocument xmlDoc = new XDocument();
                try
                {
                    xmlDoc = XDocument.Parse(sr.ReadToEnd());
                    res.icsOnlineClientKey = xmlDoc.Root.Element("icsOnlineClientKey").Value;
                    res.username = xmlDoc.Root.Element("username").Value;
                    if (xmlDoc.Root.Element("icsOnlineClientKey").Value != "001" && xmlDoc.Root.Element("icsOnlineClientKey").Value != "002" && xmlDoc.Root.Element("icsOnlineClientKey").Value != "003" && xmlDoc.Root.Element("icsOnlineClientKey").Value != "004")
                    {
                        res.isValidateByWS = true;
                    }
                    else
                    {
                        res.isValidateByWS = false;
                    }
                }
                catch (Exception)
                {
                    res.icsOnlineClientKey = "005";
                    res.username = "[none]";
                    res.isValidateByWS = false;
                }
            }
            return res;
        }

   
    
    public static string EncodePassword(string originalPassword)
        {
            try
            {
                //Declarations
                Byte[] originalBytes;
                Byte[] encodedBytes;
                MD5 md5;

                //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
                md5 = new MD5CryptoServiceProvider();
                originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
                encodedBytes = md5.ComputeHash(originalBytes);

                //Convert encoded bytes back to a 'readable' string
                return BitConverter.ToString(encodedBytes).ToLower().Replace("-", "");
            }
            catch
            {
                return "";
            }
        }     

        public static void loadRfl() {
            SvcWhitespaceClient wsClient = new SvcWhitespaceClient();
            string clientsJson = wsClient.GetChannelsList();

        }

    }

}