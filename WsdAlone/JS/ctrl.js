﻿var APP = angular.module('ICSonline', []);

userPass = {
    load: function()  {
        return localStorage['wsdjsInfo'].split('::');
    },
    save: function(u, p) {
        localStorage['wsdjsInfo'] = u + "::" + p;
    }
};

APP
.controller('topbar', ['$scope', function ($scope, $element) {
    $scope.logInOut = function() {
        if ($scope.crmLogged) {
            $scope.loggedIn = '';
            $scope.crmLogged = false;
            for (var i=0; i<GLOB.map.layers.length; i++) {
                if (typeof GLOB.map.layers[i].drawn !='undefined')
                    GLOB.map.removeLayer(GLOB.map.layers[i]);
            }
        } else {
            $scope.loading = true;
            var redir = (typeof noMVC != 'undefined' && noMVC) ? 'noMVC' : 'Auth';

            if (redir == 'noMVC' && typeof localStorage['wsdjsInfo'] != 'undefined') {
                $('#crmUser').val(userPass.load()[0]); 
                $('#crmPasswd').val(userPass.load()[1]);
            }
            var user = $('#crmUser').val(); 
            var passwd = $('#crmPasswd').val();

            var url = UT.Host + redir + '/Login?user=' + user + '&pass=' + passwd; 

            Ext.Ajax.request({
                url: url,
                success: function(data) {
                    $scope.loading = false;

                    userPass.save(user, passwd);
                    var req = new XMLHttpRequest();
                    req.open('GET', url, false);
                    req.send(null);
                    var headerConnData = null;
                    try {
                        var headerConnData = JSON.parse(req.getResponseHeader('conn'));
                        var headerAcctData = JSON.parse(req.getResponseHeader('acct'));
                        UT.apiKey          = req.getResponseHeader('apiKey');
                    } catch (e) {
                    }

                    //if (headerConnData != null || redir == 'noMVC') {
                    if (headerConnData != null ) {
                        initPlug(ICSPlugins.WSD);
                        $scope.userName = headerConnData.username;
                        $scope.crmLogged = true;
                        $scope.loggedIn = 'in';
                    } else {
                        localStorage.removeItem('wsdjsInfo');
                    }

                    $scope.$digest();
                }
            });
        }
    }

    $scope.logOut = function() {
    }
}]);

APP
.factory('loginSrv', [function() {
    return {
        logInOut: function(userName, passwd, cb) {
            var url= "http://" + document.location.host + '/Auth/Login?user=' + userName + '&pass=' + passwd; 

            Ext.Ajax.request({
                url: url,
                success: function(data) {
                    var req = new XMLHttpRequest();
                    req.open('GET', url, false);
                    req.send(null);
                    cb( JSON.parse(req.getResponseHeader('conn')),
                        JSON.parse(req.getResponseHeader('acct')));
                }
            });
        }
    }
}])
.controller('panesWrap', ['$scope', '$element', function($scope, $element) {
    $scope.updChan = function(id) { $scope.chanId = id; };
    $scope.updDev  = function(id) { $scope.devId  = id; };
    $scope.init    = function(scope) {};

    $scope.$on('itemClicked', function(evt, params) {
        $scope.$broadcast(params[0] == 'chan' ? 'clickDev' : 'clickChan', params);
    });

    $scope.toFront = function(which) {
        ActivePlugin.methods.toFront($scope.toFrontCl = which);
    }
}])
.controller('ngChannelList', ['$scope', '$element', function($scope, $element) {
    $scope.vis = {};

    $scope.short = $scope.med = $scope.long = true;

    function filterOn() {
        return $scope.short || $scope.med || $scope.long;
    };

    function setClass() {
        $scope.hideCl       = UT.js.hasVal($scope.vis, true);
        $scope.showCl       = UT.js.hasVal($scope.vis, false);
        $scope.reverseClass = $scope.showCl && $scope.hideCl;

        if ($scope.hideCl) $scope.toFront('dev');
    }

    function flipChan(chanId) {
        if (filterOn()) {
            $scope.vis[chanId] = !$scope.vis[chanId];
            setClass();
        } else {
            UT.notify('optionsOff').set('You must enable at least on of the filtering options','warn');

            var incr = 200;
            var cycle = incr * 4;
            for (var i=0; i<3; i++) {
                setTimeout(function() {$scope.short = true; $scope.$digest();}, cycle*i);
                setTimeout(function() {$scope.med = !($scope.short = false); $scope.$digest();} ,cycle*i+incr);
                setTimeout(function() {$scope.med = !($scope.long= true); $scope.$digest(); } ,cycle*i+incr*2);
                setTimeout(function() {$scope.long = false; $scope.$digest(); } ,cycle*i+incr*3);
            }
            return false;
        }
        return chanId;
    };

    $scope.init = function() {
        //$scope.showCl = !($scope.hideCl = true);
        $scope.showCl = !($scope.hideCl = false);

        for (var i=0; i<$scope.data.channels.length; i++)
            $scope.vis[$scope.data.channels[i].Channel] = $scope.hideCl;

        $scope.showAll($scope.hideCl).$digest();;
    };

    $scope.showRange = function(which) {
        ActivePlugin.methods.showRange(which, $scope[which] = !$scope[which], $scope.vis);
    };

    $scope.doReverse = function() {
        if ($scope.reverseClass) {
            for (var i in $scope.vis)
                ActivePlugin.methods.chanClick(flipChan(i), $scope.vis[i]);

            ActivePlugin.methods.redrawLayer('chan');
            $scope.chanId = 0;
        }
    };

    $scope.showAll = function(doShow) {
        doShow = typeof doShow == 'undefined' || doShow;
        //$scope.short = $scope.med = $scope.long = true;

        for (var i in $scope.vis) {
            $scope.vis[i] = doShow;
            ActivePlugin.methods.chanClick(i, doShow);
        }
        setClass();

        setTimeout(function() {ActivePlugin.methods.redrawLayer('chan'); }, 100);
        $scope.chanId = 0;
        return $scope;
    };

    $scope.$on('clickChan', function(evt, params) {
        $scope.vis[params[1]] = false;
        $scope.chanClick(params[1]);
    });

    $scope.chanClick = function(chanId) {
        ActivePlugin.methods.chanClick(flipChan(chanId), $scope.vis[chanId]);
        if ($scope.vis[chanId]) {
            $scope.vis[chanId] = 'red';
            setTimeout( function() { $scope.vis[chanId] = true; }, 200);
        }
        ActivePlugin.methods.redrawLayer('chan'); 
        $scope.chanId = $scope.vis[chanId] ? chanId : 0;
    };

}])
.controller('ngDeviceList', ['$scope', '$element', function($scope, $element) {
    $scope.reverseClass = false;
    $scope.vis = {};
    $scope.data = {};

    function setClass() {
        $scope.hideCl       = UT.js.hasVal($scope.vis, true);
        $scope.showCl       = UT.js.hasVal($scope.vis, false);
        $scope.reverseClass = $scope.showCl && $scope.hideCl;

        if ($scope.hideCl) $scope.toFront('chan');
    }

    function flipDev(devId) {
        $scope.vis[devId] = !$scope.vis[devId];
        setClass();
        return devId;
    };

    $scope.init = function() {
        $scope.showCl = !($scope.hideCl = true);
        for (var i=0; i<$scope.data.devices.length; i++)
            $scope.vis[$scope.data.devices[i].devId] = $scope.hideCl;
        
        $scope.showAll($scope.hideCl).$digest();;
    };

    $scope.doReverse = function() {
        if ($scope.reverseClass) {
            for (var i in $scope.vis)
                ActivePlugin.methods.devClick(flipDev(i), $scope.vis[i]);

            setTimeout( function() { ActivePlugin.methods.redrawLayer('dev'); }, 100);
        }
    };

    $scope.showAll = function(doShow) {
        doShow = typeof doShow == 'undefined' || doShow;
        for (var i in $scope.vis)
            ActivePlugin.methods.devClick(i, $scope.vis[i] = doShow);

        ActivePlugin.methods.redrawLayer('dev');
        setTimeout(function() { ActivePlugin.methods.redrawLayer('dev'); }, 30);

        setClass();
        $scope.devId = 0;
        return $scope;
    };

    $scope.$on('clickDev', function(evt, params) {
        $scope.devClick(params[1]);
        if ($scope.vis[params[1]] == false)
            $scope.devClick(params[1]);
    });

    $scope.devClick = function(devId) {
        ActivePlugin.methods.devClick(flipDev(devId), $scope.vis[devId]);
        if ($scope.vis[devId]) {
            $scope.vis[devId] = 'red';
            setTimeout( function() { $scope.vis[devId] = true; }, 300);
        }

        ActivePlugin.methods.redrawLayer('dev');
        $scope.devId = $scope.vis[devId] ? devId : 0;
    }
}])
.directive('ngDevice', [function() {
    return {
        restrict: 'A',
        scope: {'devid':  '=' },
        replace: false,
        template:  document.getElementById('ngdevice').innerHTML,
        compile: function(element, attributes) {
            return function($scope, el, attrs) {
                $scope.$watch('devid', function(a,b) { 
                    $scope.devData = UT.Stor('Devices').get(a);
                    el.addClass('flash');
                    setTimeout(function() { el.removeClass('flash'); }, 500);
                });
                $scope.hiChans = function() {
                    UT.notify('hichans').set('... not implemented yet');
                }
            }
        }
    }
}])
.directive('ngChannel', [function() {
    return {
        restrict: 'A',
        scope: {'chanid':  '=' },
        replace: false,
        template:  document.getElementById('ngchannel').innerHTML,
        compile: function(element, attributes) {
            return function($scope, el, attrs) {
                $scope.$watch('chanid', function(a,b) { 
                    $scope.chanData = UT.Stor('Channels').get(a);
                    el.addClass('flash');
                    setTimeout(function() { el.removeClass('flash'); }, 500);
                });
                $scope.hiChans = function() {
                    UT.notify('hichans').set('... not implemented yet');
                };
                $scope.chanDev = function(devId) {
                    LG( devId );
                }
                $scope.$watch('devclicked', function(a,b) {
                    LG('dev Clicked watch', a, b);
                });
            }
        }
    }
}])
.directive('ngButton', [function() {
    return {
        restrict: 'A',
        scope: {'item': '='},
        replace: true,
        template:  '<button ng-click="clk()" ng-class="cls">{{item}}</button>',
        compile: function(element, attributes) {
            return function($scope, el, attrs) {
                $scope.cls = 'show';
                $scope.clk = function() {
                    $scope.cls = 'red';
                    setTimeout( function() { $scope.cls = 'show'; }, 200);
                    $scope.$emit('itemClicked', [attrs.container, $scope.item]);
                }
            }
        }
    }
}])
.directive('ngnotif', [function() {
    return {
        restrict: 'C',
        replace: false,
        template:  
            '<div class="statusInfo" ng-class="msg.cls" ng-repeat="msg in messages">{{msg.msg}}</div>',
        compile: function(element, attributes) {
            return function($scope, el, attrs) {
                var loopSize    = 5;
                var durationUnit = 2000;
                $scope.messages = [];

                $scope.addMsg = function(arg) {
                    if ( $scope.messages.length >= loopSize) $scope.messages.shift();

                    $scope.messages[$scope.messages.length] = { msg: arg[0], cls: arg[1] };
                    setTimeout( function() { 
                        $scope.messages.shift(); 
                        $scope.$digest();
                    }, arg[2] * durationUnit);
                    return $scope;
                };
            }
        }
    }
}])
.filter('showOn', [function() {
    return function(input, mod) {
        if (input=='red')   return 'red';
        else                return input ? 'show' : 'noshow';
    }
}])
.filter('frontCl', [function() {
    return function(input, which) {
        return input == which ? 'show' : 'noshow';
    }
}])
;

