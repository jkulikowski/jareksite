﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using WsdAlone.MdnSvc;

namespace WsdAlone.Models
{
    public class Auth
    {
        private string GetSessToken()
        {
            return HttpContext.Current.Session["MdnTokenId"].ToString();
        }

        public string MdnLogout()
        {
            HttpContext.Current.Session["MdnTokenId"] = null;
            return "{\"message\": \"Session Ended\", \"success\" : true}";
        }

        public string MdnLogin(string username, string passwd)
        {
            MdnSrvClient srvClient = new MdnSrvClient();
            DateTime refTim = srvClient.GetServerUtc();

            HttpContext.Current.Session["MdnTokenId"] = null;

            string passTmHash = PasswordTimeHash(passwd, refTim);
            try
            {
                UserProfile uws = srvClient.Login(username, refTim, passTmHash);
                HttpContext.Current.Session["MdnTokenId"] = uws.TokenId;
            }
            catch { }

            string res = "{\"success\": " + (HttpContext.Current.Session["MdnTokenId"] == null ? "false" : "true") + "}";
            return res;
        }

        public string SHA256Hash(string input)
        {
            byte[] inputbytes = UTF8Encoding.UTF8.GetBytes(input);
            SHA256 sha = new SHA256CryptoServiceProvider();
            byte[] hash = sha.ComputeHash(inputbytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++) sb.Append(hash[i].ToString("X2"));
            return sb.ToString();
        }
        public string Stringify(DateTime tm)
        {
            string ret = ((((((tm.Year - 2000) * 12 + tm.Month) * 31 + tm.Day) * 24 + tm.Hour) * 60 + tm.Minute) * 60 + tm.Second).ToString();
            return ret;
        }
        public string PasswordTimeHash(string password, DateTime refTime)
        {
            string refTimeS = Stringify(refTime);
            string passHash = SHA256Hash(password);
            return SHA256Hash(refTimeS + passHash);
        }
    }
}