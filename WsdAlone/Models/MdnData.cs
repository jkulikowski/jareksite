﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsdAlone.Models
{
    public class MdnData<L,D>
    {
        public string count;
        public L[] list;
        public D detail;
        public List<D> details = new List<D>();
    }
}