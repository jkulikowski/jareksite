﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WsdAlone.brific;
using WsdAlone.brificToAzure;
using WsdAlone.WhiteSvc;
using WsdAlone.Models;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace WsdAlone.Controllers
{
    public class ApiController : Controller
    {
        //
        // GET: /Api/
        public ActionResult Index()
        {
            brific.CountryNamesAndCodes br = new brific.CountryNamesAndCodes();
            brificToAzure.BrificToAzureServiceClient btaClient = new brificToAzure.BrificToAzureServiceClient();
            ViewBag.hash = btaClient.GetHashCode().ToString();

            ViewBag.message = "{pos1: \"message from controller Api\"}";
            return View();
        }
        public ActionResult GetKey()
        {
            string apiKey = HttpContext.Session["key"].ToString();
            return Content(apiKey.ToString());
        }
        public ActionResult Test()
        {
            return Content("test api");
        }


        public ActionResult WsChannelsList(string k = "")
        {
            // Authenticate against Whitespace plugin
            if (SvcAuth.HasPlugCreds(Request.QueryString["k"]))
                return Content("Your key is invalid");
            else
                return Content((new SvcWhitespaceClient()).GetChannelsList());
        }

        // Devices at point
        public ActionResult WsTvwsLookup(string k = "", string lo = "0", string la = "0")
        {
            try
            {
                var Sess = HttpContext.Session;
                string sessKey = HttpContext.Session["key"].ToString();
                string sess1Key = HttpContext.Session["key"].ToString();
            }
            catch            {
                //return Content( e.Message.ToString());
            }

            double lon = Convert.ToDouble(Request.QueryString["lo"]);
            double lat = Convert.ToDouble(Request.QueryString["la"]);
            string res = (new SvcWhitespaceClient()).TvwsLookup(lon, lat);
            /*
            var jsonObj = JsonConvert.DeserializeObject<dynamic>(res);
            List<string> ids = new List<string>();
            for (int i = 0; i < jsonObj.length; i++)
                ids.Add(jsonObj[i].Channel.ToString());
            */   
            return Content(res);
        }

        public ActionResult WsDeviceList(string k = "", string lonMin = "0", string lonMax = "20", string latMin = "0", string latMax = "0", string chanList = "21,22")
        {
            Dictionary<string, double> q = toDbl((NameValueCollection)Request.QueryString);
            string res = (new SvcWhitespaceClient()).GetDevicesList(q["lonMin"], q["lonMax"], q["latMin"], q["latMax"], null);
            //string res = (new SvcWhitespaceClient()).GetDevicesList(0.0, 88.0, 0.0, 88.0, null);
            return Content(res);
        }

        public ActionResult WsDeviceDetails(string ids)
        {
            string idList = Request.QueryString["ids"];
            return Content(getDeviceDetails(idList));
        }

        public ActionResult WsDeviceContoursMock(string k = "", string ids = "")
        {
            string idList = Request.QueryString["ids"];
            string[] idsA = idList.Split(',');

            string res = getDeviceDetails(idList);
            return Content(res);
        }
        public ActionResult WsChannelContoursMock(string k = "", string ids = "")
        {
            string channelsList = (new SvcWhitespaceClient()).GetChannelsList();
            string res = channelsList;
            return Content(res);
        }

        private string getDeviceDetails(string idList)
        {
            string[] idsA = idList.Split(',');

            int[] idsI = new int[idsA.Length];
            for (int i = 0; i < idsA.Length; i++)
            {
                idsI[i] = Convert.ToInt32(idsA[i]);
            }
            int[] idsII = new int[60];
            for (int i = 0; i < 60; i++)
            {
                idsII[i] = i;
            }
            string res = (new SvcWhitespaceClient()).GetDevicesDetails(idsII);

            return res;
        }

        private Dictionary<string, double> toDbl(NameValueCollection qStr)
        {
            Dictionary<string, double> ret = new Dictionary<string, double>();
            foreach (var k in qStr.AllKeys)
            {
                if (qStr[k] != "" && k == "lonMax" || k == "lonMin" || k == "latMax" || k == "latMin")
                    ret.Add(k, Convert.ToDouble(qStr[k]));
            }
            return ret;
        }
    }
}
