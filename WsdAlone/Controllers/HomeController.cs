﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WsdAlone.Models;


namespace WsdAlone.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Map");
            //return View();
        }

        public ActionResult About()
        {
            Session["isAuth"] = false;
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ServiceUtils.loadRfl();
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Raw()
        {
            return Content("{json: \"here\"}");
        }
    }
}